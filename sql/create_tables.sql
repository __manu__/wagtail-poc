CREATE TABLE IF NOT EXISTS "public"."userofferdata" (
  "id" INTEGER NOT NULL ,
  "userid" VARCHAR(100) NULL,
  "offerids" VARCHAR(100) NULL,
  CONSTRAINT "userofferdata_pkey" PRIMARY KEY ("id")
);


CREATE TABLE  IF NOT EXISTS "public"."offerdata" (
  "offerid" INTEGER NOT NULL,
  "name" VARCHAR(100) NULL,
  "tag" VARCHAR(100) NULL,

  CONSTRAINT "offerdata_pkey" PRIMARY KEY ("offerid")
);
