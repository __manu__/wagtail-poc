# Wagtail PoC

The objective of the PoC is to setup a CMS application with database dashboard for basic database table management.

## Stack

- Django
- Wagtail CMS
- Docker
- Postgres

## How to Setup

- Clone the repo
- Run `docker-compose up -d`
- Go to `http://localhost:9007`
    - Login with username `test` and password `test123`
    
## Configuring user

- Currently admin user of the app is created during docker build. Refer last line in `Dockerfile`


## How to add new database and tables

- Go to `src\settings\base.py`
- Look for `DATABASES`
- Add new entry in JSON
- `python manage.py inspectdb --database <database name as configured in JSON>`
- Copy the generated model as required to `src\home\models.py`
- Add admin configuration as configured for other entities
- Add entry in `src\settings\base.py` > `DATABASE_MODEL_TABLE`