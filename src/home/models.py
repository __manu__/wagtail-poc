from django.db import models
from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register
from wagtail.core.models import Page


class HomePage(Page):
    pass


class Offerdata(models.Model):
    offerid = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    tag = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'offerdata'


class UserOfferData(models.Model):
    id = models.IntegerField(primary_key=True)
    userid = models.CharField(max_length=100, blank=True, null=True)
    offerids = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'userofferdata'


class Offer(ModelAdmin):
    model = Offerdata
    menu_label = "Offers"
    menu_icon = "pick"
    menu_order = 200
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ("offerid", "name", "tag")
    list_filter = ("name",)
    search_fields = ("name",)


class UserOfferDataAdmin(ModelAdmin):
    model = UserOfferData
    menu_label = "Users & Offers"
    menu_icon = "pick"
    menu_order = 201
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ("id", "userid")
    list_filter = ("userid",)
    search_fields = ("userid",)


modeladmin_register(Offer)
modeladmin_register(UserOfferDataAdmin)
