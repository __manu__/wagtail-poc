FROM python:3.7
ENV PYTHONUNBUFFERED 1
RUN mkdir /app
WORKDIR /app
ADD ./src /app
RUN pip install -r requirements.txt
RUN python manage.py superuser --username test --password test123 --noinput --email 'blank@email.com'
